# Gallery Gallery exhibition base

This is a base project for your [Gallery Gallery](https://www.gallerygallery.space) exhibition.

The exhibition will be hosted on our local router using a modified version of [Piratebox](https://www.piratebox.cc/doku.php).

Since the router is not connected to the internet, the entire exhibition needs to be contained inside this folder. That means that every file you need for your show has to be inide your exhibition folder.

This base is only a suggested setup to help you get started. You are, of course, free to deviate from this.

## Structure of this exhibition base

The structure of the exhibition base is as follows:

- assets
  - art
  - fonts
  - images
  - gallerygallery.css
- favicon.ico
- index.html
- open_browser.html
- README.md

### Assets
The `assets` folder is used to hold any assets, such as images, audio, video or js files used in the project

The `fonts` folder is where our house style font [IA DuoSpace](https://github.com/iaolo/iA-Fonts/tree/master/iA%20Writer%20Duospace) is stored. This font is used in the default `open_browser.html` and `index.html` files.

The `images` folder is where we store images and logos needed for specific exhibitions, such a logos from sponsors or associated if your exhibition participates in a wider event, and so on.

The `art` folder is where your stuff goes.

The stylesheet `gallerygallery.css` contains our housestyle css code, used in the default `open_browser.html` and `index.html` files.

### Favicon
The file `favicon.ico` is our default favicon file.

### Index.html
The `index.html` file is the "homepage" of your exhibiiton. It contains a default layout with a few images and videos, just to get you started and give you some ideas, but here you are free to do anything you want.

Think of this as our white gallery wall :)

You can, of course, create as many additional pages as you like.

### open_browser.html
The `open_browser.html` file is used for the captive screen when mobile devices first connect to the router. Not all devices, however, show this screen, especially on Android this is a problem.

website: [htps://www.gallerygallery.space](htps://www.gallerygallery.space)
Instagram: gallery_gallery_space
